#include "gbitarray.h"

gint
main (gint argc,
      gchar *argv[])
{
   GBitArray *bitarray = g_bit_array_new(1024);
   guint i;
   guint j;

   for (i = 0; i < 1024; i++) {
      g_bit_array_set_bit(bitarray, i, TRUE);
      for (j = 0; j < 1024; j++) {
         if (j != i) {
            g_assert_cmpint(FALSE, ==, g_bit_array_get_bit(bitarray, j));
         } else {
            g_assert_cmpint(TRUE, ==, g_bit_array_get_bit(bitarray, j));
         }
      }

      g_bit_array_set_bit(bitarray, i, FALSE);
      for (j = 0; j < 1024; j++) {
         g_assert_cmpint(FALSE, ==, g_bit_array_get_bit(bitarray, j));
      }
   }

   return 0;
}
