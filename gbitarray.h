/* gbitarray.h
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef G_BIT_ARRAY_H
#define G_BIT_ARRAY_H

#include <glib.h>

G_BEGIN_DECLS

typedef struct _GBitArray GBitArray;

struct _GBitArray
{
   gint   ref_count;
   gint   width;
   guint8 bitarray[0];
};

GBitArray *g_bit_array_new   (gint       width);
GBitArray *g_bit_array_ref   (GBitArray *bitarray);
void       g_bit_array_unref (GBitArray *bitarray);

static inline gboolean
g_bit_array_get_bit (GBitArray *bitarray,
                     gint       bit)
{
   gint byte;

   g_return_val_if_fail(bitarray != NULL, FALSE);
   g_return_val_if_fail(bit < bitarray->width, FALSE);

   byte = bit / sizeof(guint8);
   bit %= sizeof(guint8);
   return !!(bitarray->bitarray[byte] & (1 << bit));
}

static inline void
g_bit_array_set_bit (GBitArray *bitarray,
                     gint       bit,
                     gboolean   bit_set)
{
   gint byte;

   g_return_if_fail(bitarray != NULL);
   g_return_if_fail(bit < bitarray->width);

   byte = bit / sizeof(guint8);
   bit %= sizeof(guint8);

   if (bit_set) {
      bitarray->bitarray[byte] |= (1 << bit);
   } else {
      bitarray->bitarray[byte] &= ~(1 << bit);
   }
}

G_END_DECLS

#endif /* G_BIT_ARRAY_H */
