/* gbitarray.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gbitarray.h"

GBitArray *
g_bit_array_new (gint width)
{
   GBitArray *bitarray;

   bitarray = g_malloc0((sizeof *bitarray) + width);
   bitarray->ref_count = 1;
   bitarray->width = width;
   return bitarray;
}

GBitArray *
g_bit_array_ref (GBitArray *bitarray)
{
   g_return_val_if_fail(bitarray != NULL, NULL);
   g_return_val_if_fail(bitarray->ref_count > 0, NULL);

   g_atomic_int_inc(&bitarray->ref_count);
   return bitarray;
}

void
g_bit_array_unref (GBitArray *bitarray)
{
   g_return_if_fail(bitarray != NULL);
   g_return_if_fail(bitarray->ref_count > 0);

   if (g_atomic_int_dec_and_test(&bitarray->ref_count)) {
      g_free(bitarray);
   }
}
