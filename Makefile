all: gbitarray

WARNINGS =
WARNINGS += -Wall
WARNINGS += -Werror

gbitarray: gbitarray.h gbitarray.c tests.c
	$(CC) -o $@ $(shell pkg-config --cflags --libs glib-2.0) gbitarray.c tests.c $(WARNINGS)

clean:
	rm -f gbitarray
